#!/usr/bin/env python3
import requests
import re
import time
import sys

dic = sys.argv[1]
neplout = sys.argv[2]


s = requests.Session()

# Target Address: https://adisspr.mfcr.cz/dpr/DphReg
pagehead = "https://adisspr.mfcr.cz"
dpr = s.get(pagehead+"/dpr/DphReg")

state = re.findall(
    b'id="javax\.faces\.ViewState" value="(.*)" />', dpr.content)[0]
form = {"form:dt:0:inputDic": dic, "form:hledej": "Hledej++",
        "autoScroll": "", "form:nespolehlivost": "0", "form:pocet_radek": "1",
        "form_SUBMIT": "1", "form:_idcl": "", "form:_link_hidden_": "",
        "javax.faces.ViewState": state.decode("utf-8")}

time.sleep(2)
dpr_output = s.post(
    'https://adisspr.mfcr.cz/dpr/adis/irs/irep_dph/dphInputForm.faces',
    data=form)

nespoleh = re.search(b'Nespolehliv.*"data">([A-Z]*).*?</td>',
                     dpr_output.content)[1].decode("utf-8")

if nespoleh == "NE":
    rawaccounts = re.search(
            b'<tbody id="tableUcty\[0\]:tbody_element">.*?</tbody>', 
            dpr_output.content, re.DOTALL)[0]
    accounts = [i.decode("utf-8") for i in
        re.findall(b'<span class="data">(.*?)</span>', rawaccounts)[::2]]
    outstr = f'{dic:<12}N01.01.0001'+",".join(accounts)
else:
    datum = re.search(b'Nespolehliv.*"data">.*?: (.*)</td>',
                      dpr_output.content)[1].decode("utf-8")
    outstr = f'{dic:<12} A{datum}'

with open(neplout, "w") as f:
    f.write(outstr)
