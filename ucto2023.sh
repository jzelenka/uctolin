#!/bin/bash

UCTODIR=/home/yan/documents/osvc_zivnost_cz/ucto2000
UCTOVER=UCTO2023

cd $UCTODIR
dosbox -conf dosboxucto.conf &

# linuxova implementace UBOX

echo "spoustim smycku sluzeb na pozadi"
while :; do
        sleep 1
        if ps -A | grep -q "dosbox"; then
                sleep 1
        else
                echo "umiram"
                exit 0
        fi
        odltask=""
        task=$(tail -n1 $UCTOVER/\{dbx1\}/DBX2.LOG | sed 's/\\/\\\\/g')
        if [ "$task" != "$oldtask" ]; then
                oldtask="$task"
                command=$(awk -v var="$task" 'BEGIN {split (var,a); print a[3];}')
                echo "$command"
                if [[ "$command"  == "@D:\\UCTO2023\\{TISK}\\UCTOARES.EXE" ]]; then
                        ICO=$(awk -v var="$task" 'BEGIN {split (var,a); print a[5];}')
                        echo "Hledam ICO $ICO v databazi ARES"
                        UCTOTXT=$UCTODIR/$UCTOVER/UCTOTXT.UUU
                        ./ares.py $ICO $UCTOTXT
                elif [[ "$command" == "@D:\\UCTO2023\\{AP02}\\NEPL2.EXE" ]]; then
                        echo "Hledam DIC $DIC v databazi subjektu DPH"
                        DIC=`cat $UCTODIR/$UCTOVER/{ap02}/NEPLIN.TXT`
                        NEPLOUT=$UCTODIR/$UCTOVER/{ap02}/NEPLOUT.TXT
                        ./nepl.py $DIC $NEPLOUT
                fi
        fi
done
