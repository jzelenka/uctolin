#!/usr/bin/env python3
import urllib.request
import xml.etree.ElementTree as ET
import sys

ico = sys.argv[1]
uctotxt = sys.argv[2]

webUrl = urllib.request.urlopen(
        f"http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico={ico}&ver=1.0.2")
data = ET.fromstring(webUrl.read())
keys = ["Obchodni_firma", "Nazev_ulice", "Cislo_domovni", "Nazev_obce", "PSC",
        "ICO", "DIC", "Kod_PF", "Priznaky_subjektu"] 
ares_dict = dict.fromkeys(keys, "")
for val in data.iter():
    if val.tag.endswith("Pocet_zaznamu") and val.text == "0":
        quit()
    for i in keys:
        if val.tag.endswith(i):
            ares_dict[i] = val.text

#print(ares_dict)
a = ares_dict
outstr = (f'{a["Obchodni_firma"]}\r'
          f'{a["Nazev_ulice"]} {a["Cislo_domovni"]}\r'
          f'{a["Nazev_obce"]}\r'
          f'{a["PSC"]}\r'
          f'{a["ICO"]}\r'
          f'{a["DIC"]}\r'
          f'{a["Kod_PF"]}\r'
          f'{a["Priznaky_subjektu"][5]}')
with open(uctotxt, "wb") as f:
    f.write(outstr.encode("cp852"))
